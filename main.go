package main

import (
	handlers "./handlers"
	"net/http"
)

func main() {
	
	

	http.HandleFunc("/deleteuser/deleted", handlers.DeleteRow)
	http.HandleFunc("/deleteuser/", handlers.DeleteRowServ)

	http.HandleFunc("/", handlers.PostDataFunc)

	http.HandleFunc("/show", handlers.GetDataFunc)



	http.ListenAndServe(":8080", nil)
}